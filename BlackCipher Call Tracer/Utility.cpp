#include "Utility.hpp"
#include <string>

#pragma region Generic Code
namespace Utility {
	bool bCompare( const char * pData, const char * bMask, const char * szMask )
	{
		for ( ; *szMask; ++szMask, ++pData, ++bMask )
			if ( *szMask == 'x' && *pData != *bMask )
				return false;
		return ( *szMask ) == NULL;
	}

	uintptr_t FindPattern( uintptr_t dwAddress, uintptr_t dwLen, const char * bMask, const char * szMask )
	{
		if ( dwAddress == 0 )
		{
			//Log( "Start address is null" );
			return 0;
		}

		for ( uintptr_t i = 0; i < dwLen; i++ )
			if ( bCompare( ( char * )( dwAddress + i ), bMask, szMask ) )
				return ( uintptr_t )( dwAddress + i );

		return 0;
	}

	uintptr_t FindPattern( uintptr_t dwAddress, uintptr_t dwLen, char * pat )
	{
		std::string pattern( pat ), bytePattern, byteMask;
		// AA BB CC DD EE ?? FF GG ? ZZ
		// do this past each space. strtol will stop at that point.
		auto loc = 0;
		while ( loc != pattern.npos )
		{
			// case for ?? / ?
			if ( *( pattern.c_str( ) + loc ) == '?' )
				bytePattern += '\x00', byteMask += '?';
			else
				bytePattern += strtol( pattern.c_str( ) + loc, 0, 16 ), byteMask += 'x';

			auto f = pattern.find( ' ', loc );
			loc = f != pattern.npos ? f + 1 : f; //npos or space + 1
		}

		return FindPattern( dwAddress, dwLen, bytePattern.c_str( ), byteMask.c_str( ) );
	}
}
#pragma endregion
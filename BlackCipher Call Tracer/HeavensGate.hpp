#include "generic_header.hpp"
#include <ntdll.h>

namespace HeavensGate
{
	struct CONTEXT64
	{
		uint64_t P1Home;
		uint64_t P2Home;
		uint64_t P3Home;
		uint64_t P4Home;
		uint64_t P5Home;
		uint64_t P6Home;
		uint32_t ContextFlags;
		uint32_t MxCsr;
		uint16_t SegCs;
		uint16_t SegDs;
		uint16_t SegEs;
		uint16_t SegFs;
		uint16_t SegGs;
		uint16_t SegSs;
		uint32_t EFlags;
		uint64_t Dr0;
		uint64_t Dr1;
		uint64_t Dr2;
		uint64_t Dr3;
		uint64_t Dr6;
		uint64_t Dr7;
		uint64_t Rax;
		uint64_t Rcx;
		uint64_t Rdx;
		uint64_t Rbx;
		uint64_t Rsp;
		uint64_t Rbp;
		uint64_t Rsi;
		uint64_t Rdi;
		uint64_t R8;
		uint64_t R9;
		uint64_t R10;
		uint64_t R11;
		uint64_t R12;
		uint64_t R13;
		uint64_t R14;
		uint64_t R15;
		uint64_t Rip;
	};

	// TODO: Get 64 bit PEB, get ntdll/kernel32, walk module for exports, get functions desired, return address.
	extern "C" void __cdecl SetupLongHandler( uint64_t RtlExceptionHandlerFn );

	struct PEB64;
	uint64_t GetPEB64( );
	// doesnt work on 32 bit modules.
	bool RemoveFromPEB64( uintptr_t base );
	bool RemoveFromPEB( uintptr_t base );
	uint64_t GetModule( PEB64 * pPEB, const wchar_t * mod );
	uint64_t GetExport( uint64_t mod, const char * name );
	void Setup64BitExceptionHandler( );
}
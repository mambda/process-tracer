.model flat, c


EXTERN HeavensBridge:PROC
PUBLIC module_start
PUBLIC module_size
PUBLIC api_count
PUBLIC api_array
PUBLIC arg_array


heaven SEGMENT execute write 

; Enter heavens gate, setup exception handler, exception handler can be DB'ed from this DLL's basichandler?
HG_ENTER MACRO
push 033h
call $+5
add dword ptr[esp], 5
retf
; WE HAVE ENTERED HG.
ENDM

HG_END MACRO
call $+5
db 0C7h, 44h, 24h, 04h, 23h, 0, 0, 0 ; mov dword [rsp+4], 0x23
db 83h, 04h, 24h, 0Dh, 0CBh ; add dword [rsp], 0xD. RETF
ENDM

api_array dq 5 dup(0)
arg_array dq 5*4 dup(0)
api_count dd 0
module_start dd 0
module_size dd 0
potato_fn dq 0 ; lol.

x64Handler proc

db 051h, 041h, 050h, 053h, 056h, 041h, 057h, 041h, 056h, 041h, 055h, 041h, 054h, 041h, 053h, 041h, 052h, 057h, 04Ch, 08Bh, 001h, 048h, 08Bh, 049h, 008h, 041h, 08Bh, 000h, 03Dh, 004h
db 000h, 000h, 080h, 00Fh, 085h, 018h, 001h, 000h, 000h, 048h, 08Bh, 081h, 0F8h, 000h, 000h, 000h, 03Bh, 005h, 0BCh, 0FFh, 0FFh, 0FFh, 07Ch, 04Fh, 08Bh, 035h, 0B4h, 0FFh, 0FFh, 0FFh
db 003h, 035h, 0B2h, 0FFh, 0FFh, 0FFh, 048h, 039h, 0F0h, 07Fh, 03Eh, 048h, 08Bh, 059h, 048h, 048h, 039h, 0D8h, 075h, 00Bh, 048h, 031h, 0DBh, 048h, 089h, 059h, 048h, 048h, 089h, 059h
db 070h, 048h, 08Bh, 059h, 070h, 048h, 0C7h, 0C6h, 001h, 000h, 000h, 000h, 048h, 0C1h, 0E6h, 009h, 048h, 009h, 0F3h, 048h, 089h, 059h, 070h, 048h, 08Bh, 059h, 044h, 048h, 081h, 0CBh
db 000h, 001h, 000h, 000h, 048h, 089h, 059h, 044h, 0E9h, 0A2h, 000h, 000h, 000h, 048h, 08Bh, 059h, 070h, 048h, 031h, 0DBh, 089h, 059h, 070h, 048h, 08Bh, 059h, 044h, 048h, 0C7h, 0C6h
db 000h, 001h, 000h, 000h, 048h, 0F7h, 0D6h, 048h, 021h, 0F3h, 048h, 089h, 059h, 044h, 051h, 041h, 050h, 053h, 056h, 041h, 057h, 041h, 056h, 041h, 055h, 041h, 054h, 041h, 053h, 041h
db 052h, 057h, 04Ch, 08Bh, 091h, 0C8h, 000h, 000h, 000h, 04Ch, 08Bh, 099h, 0D0h, 000h, 000h, 000h, 04Ch, 08Bh, 0A1h, 0D8h, 000h, 000h, 000h, 04Ch, 08Bh, 0A9h, 0E0h, 000h, 000h, 000h
db 04Ch, 08Bh, 0B1h, 0E8h, 000h, 000h, 000h, 04Ch, 08Bh, 0B9h, 0F0h, 000h, 000h, 000h, 048h, 081h, 0ECh, 000h, 001h, 000h, 000h, 067h, 089h, 00Ch, 024h, 048h, 08Dh, 005h, 006h, 0FFh
db 0FFh, 0FFh, 0FFh, 010h, 048h, 081h, 0C4h, 000h, 001h, 000h, 000h, 05Fh, 041h, 05Ah, 041h, 05Bh, 041h, 05Ch, 041h, 05Dh, 041h, 05Eh, 041h, 05Fh, 05Eh, 05Bh, 041h, 058h, 059h, 048h
db 08Bh, 099h, 098h, 000h, 000h, 000h, 048h, 08Bh, 01Bh, 048h, 089h, 059h, 048h, 048h, 08Bh, 059h, 070h, 048h, 083h, 0CBh, 001h, 048h, 089h, 059h, 070h, 05Fh, 041h, 05Ah, 041h, 05Bh
db 041h, 05Ch, 041h, 05Dh, 041h, 05Eh, 041h, 05Fh, 05Eh, 05Bh, 041h, 058h, 059h, 0B8h, 0FFh, 0FFh, 0FFh, 0FFh, 0C3h, 05Fh, 041h, 05Ah, 041h, 05Bh, 041h, 05Ch, 041h, 05Dh, 041h, 05Eh
db 041h, 05Fh, 05Eh, 05Bh, 041h, 058h, 059h, 0B8h, 000h, 000h, 000h, 000h, 0C3h

x64Handler endp 

AidsTest proc

; so, we go into 32 bit mode.
HG_END

push [esp+8]
call HeavensBridge
add esp, 4

HG_ENTER

ret

AidsTest endp

SetupLongHandler proc
mov dword ptr [potato_fn], AidsTest

HG_ENTER

; [STACK] = pointer to parameters ( AddVectoredExceptionHandler in this case. )
; mov rax, dword ptr [rsp+4]
; mov rcx, 1
; mov rdx, <X64_HANDLER_FUNCTION>
; sub rsp, 20h
; call rax
; add rsp, 20h


db 048h, 08Bh, 044h, 024h, 04h
db 048h, 0C7h, 0C1h, 001h, 000h, 000h, 000h
db 048h, 0BAh
dq x64Handler
db 048h, 083h, 0ECh, 020h
db 0FFh, 0D0h
db 48h, 83h, 0C4h, 20h

HG_END ; is this wise? Should i leave it? No clue.
ret

SetupLongHandler endp

END
#include "HeavensGate.hpp"

namespace HeavensGate
{
	uint64_t GetPEB64( )
	{
		auto ntdll = GetModuleHandleA( "ntdll" );
		auto NtWow64QueryInformationProcess64 = ( decltype( NtQueryInformationProcess ) * )GetProcAddress( ntdll, "NtWow64QueryInformationProcess64" );
		struct PROCESS_BASIC_INFORMATION64
		{
			NTSTATUS ExitStatus;
			uint64_t PebBaseAddress;
			uint64_t AffinityMask;
			KPRIORITY BasePriority;
			uint64_t UniqueProcessId;
			uint64_t InheritedFromUniqueProcessId;
		};

		PROCESS_BASIC_INFORMATION64 pbi = { 0 };
		ULONG ret = 0;
		auto stat = NtWow64QueryInformationProcess64( ( HANDLE )-1, ProcessBasicInformation, &pbi, sizeof( pbi ), &ret );
		if ( stat != 0 )
		{
			Log( "Failed to query our peb." );
			return 0;
		}

		return pbi.PebBaseAddress;
	}

	struct PEB_LDR_DATA64 {
		BYTE       Reserved1[ 8 ];
		uint64_t      Reserved2[ 3 ];
		LIST_ENTRY64 InMemoryOrderModuleList;
	};

	struct PEB64 {
		BYTE                          Reserved1[ 2 ];
		BYTE                          BeingDebugged;
		BYTE                          Reserved2[ 1 ];
		uint64_t                         Reserved3[ 1 ];
		uint64_t				ImageBase;
		uint64_t				Ldr;
		uint64_t  						ProcessParameters;
		uint64_t                         Reserved4[ 3 ];
		uint64_t                         AtlThunkSListPtr;
		uint64_t                         Reserved5;
		ULONG                         Reserved6;
		uint64_t                         Reserved7;
		ULONG                         Reserved8;
		ULONG                         AtlThunkSListPtr32;
		uint64_t                         Reserved9[ 45 ];
		BYTE                          Reserved10[ 96 ];
		uint64_t 						PostProcessInitRoutine;
		BYTE                          Reserved11[ 128 ];
		uint64_t                         Reserved12[ 1 ];
		ULONG                         SessionId;
	};

	struct LDR_DATA_TABLE_ENTRY64 {
		LIST_ENTRY64 InLoadOrderLinks;
		LIST_ENTRY64 InMemoryOrderLinks;
		LIST_ENTRY64 InInitializationOrderLinks;
		uint64_t DllBase;
		PVOID64 EntryPoint;
		PVOID64 Reserved3;

		struct UNICODE_STRING64
		{
			USHORT Length;
			USHORT MaximumLength;
			DWORD PAD;
			PWSTR  Buffer;
		};
		UNICODE_STRING64 FullDllName;
	};

	template <typename t>
	bool read( uint64_t addr, t * buffer ) noexcept
	{
#undef max
		if ( addr < std::numeric_limits<uint32_t>( ).max( ) )
		{
			memcpy( buffer, ( void * )addr, sizeof( t ) );
		}
		else
		{
			// need rpm.
			auto ntdll = GetModuleHandleA( "ntdll" );
			auto NtWow64ReadVirtualMemory64 = ( long( __stdcall * )( HANDLE, uint64_t, void *, uint64_t, uint64_t * ) )GetProcAddress( ntdll, "NtWow64ReadVirtualMemory64" );
			auto handle = OpenProcess( PROCESS_ALL_ACCESS, 0, GetCurrentProcessId( ) );

			uint64_t ret = 0;
			auto stat = NtWow64ReadVirtualMemory64( handle, addr, buffer, sizeof( t ), &ret );
			CloseHandle( handle );
			if ( stat != 0 )
			{
				Log( "Failed to read" );
				return false;
			}
		}

		return true;
	}

	template <typename t>
	bool write( uint64_t addr, t * buffer ) noexcept
	{
#undef max
		if ( addr < std::numeric_limits<uint32_t>( ).max( ) )
		{
			memcpy( ( void * )addr, ( void * )buffer, sizeof( t ) );
		}
		else
		{
			// need rpm.
			auto ntdll = GetModuleHandleA( "ntdll" );
			auto NtWow64WriteVirtualMemory64 = ( long( __stdcall * )( HANDLE, uint64_t, void *, uint64_t, uint64_t * ) )GetProcAddress( ntdll, "NtWow64WriteVirtualMemory64" );
			auto handle = OpenProcess( PROCESS_ALL_ACCESS, 0, GetCurrentProcessId( ) );

			uint64_t ret = 0;
			auto stat = NtWow64WriteVirtualMemory64( handle, addr, buffer, sizeof( t ), &ret );
			CloseHandle( handle );
			if ( stat != 0 )
			{
				Log( "Failed to write" );
				return false;
			}
		}

		return true;
	}

	bool RemoveFromPEB64( uintptr_t base )
	{
		auto pPEB = ( PEB64 * )GetPEB64( );

		PEB_LDR_DATA64 ldr = { 0 };
		read<PEB_LDR_DATA64>( pPEB->Ldr, &ldr );
		auto cur = ldr.InMemoryOrderModuleList.Flink;
		auto start = CONTAINING_RECORD( cur, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );
		auto structure_offset = CONTAINING_RECORD( 0, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );
		//Log( "BASE of %S AT %llx", start->FullDllName.Buffer, start->DllBase );
		LDR_DATA_TABLE_ENTRY64 next = *start;
		do
		{
			read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InMemoryOrderLinks.Flink, &next );
			//next = CONTAINING_RECORD( &next, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );
			Log( "Current base: %p", next.DllBase );

			if ( next.DllBase == base )
			{
				Log( "Found %S in PEB64.", next.FullDllName.Buffer );
				// zero it out or replace flinks and blinks.
				LDR_DATA_TABLE_ENTRY64 prev_entry = { 0 }, next_entry = { 0 };

				// Load Order
				auto forward_link = next.InLoadOrderLinks.Flink;
				auto backward_link = next.InLoadOrderLinks.Blink;
				read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InLoadOrderLinks.Blink, &prev_entry );
				read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InLoadOrderLinks.Flink, &next_entry );
				prev_entry.InLoadOrderLinks.Flink = forward_link;
				next_entry.InLoadOrderLinks.Flink = backward_link;
				write<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InLoadOrderLinks.Blink, &prev_entry );
				write<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InLoadOrderLinks.Flink, &next_entry );

				// Mem Order
				forward_link = next.InMemoryOrderLinks.Flink;
				backward_link = next.InMemoryOrderLinks.Blink;
				read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InMemoryOrderLinks.Blink, &prev_entry );
				read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InMemoryOrderLinks.Flink, &next_entry );
				prev_entry.InMemoryOrderLinks.Flink = forward_link;
				next_entry.InMemoryOrderLinks.Flink = backward_link;
				write<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InMemoryOrderLinks.Blink, &prev_entry );
				write<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InMemoryOrderLinks.Flink, &next_entry );

				// Init Order
				forward_link = next.InInitializationOrderLinks.Flink;
				backward_link = next.InInitializationOrderLinks.Blink;
				read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InInitializationOrderLinks.Blink, &prev_entry );
				read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InInitializationOrderLinks.Flink, &next_entry );
				prev_entry.InInitializationOrderLinks.Flink = forward_link;
				next_entry.InInitializationOrderLinks.Flink = backward_link;
				write<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InInitializationOrderLinks.Blink, &prev_entry );
				write<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )structure_offset + next.InInitializationOrderLinks.Flink, &next_entry );

				Log( "Everything should be written back now. Good Luck!" );
			}

			if ( next.InMemoryOrderLinks.Flink == start->InMemoryOrderLinks.Blink )
				break;
		} while ( true );

		return false;
	}

	bool RemoveFromPEB( uintptr_t base )
	{
		auto pPEB = ( PEB * )__readfsdword( 0x30 );
		auto ldr = pPEB->Ldr;
		auto cur = ldr->InMemoryOrderModuleList.Flink;
		auto start = CONTAINING_RECORD( cur, LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks );
		auto mem_offset = ( uintptr_t )CONTAINING_RECORD( 0, LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks );
		auto load_offset = ( uintptr_t )CONTAINING_RECORD( 0, LDR_DATA_TABLE_ENTRY, InLoadOrderLinks );
		auto init_offset = ( uintptr_t )CONTAINING_RECORD( 0, LDR_DATA_TABLE_ENTRY, InInitializationOrderLinks );
		//Log( "BASE of %S AT %llx", start->FullDllName.Buffer, start->DllBase );
		LDR_DATA_TABLE_ENTRY * next = start;
		do
		{
			Log( "Current base: %p", next->DllBase );
			Log( "Structure offset is %X", mem_offset );
			Log( "next == %p", next );
			next = ( LDR_DATA_TABLE_ENTRY * )( ( uintptr_t )next->InMemoryOrderLinks.Flink + mem_offset );

			if ( ( uintptr_t )next->DllBase == base )
			{
				Log( "Found %S in PEB64.", next->FullDllName.Buffer );
				// zero it out or replace flinks and blinks.
				LDR_DATA_TABLE_ENTRY * prev_entry = 0, * next_entry = 0;

				// Load Order
				auto forward_link = next->InLoadOrderLinks.Flink;
				auto backward_link = next->InLoadOrderLinks.Blink;
				prev_entry = ( LDR_DATA_TABLE_ENTRY * )(  ( uintptr_t )next->InLoadOrderLinks.Blink + load_offset );
				next_entry = ( LDR_DATA_TABLE_ENTRY * )(  ( uintptr_t )next->InLoadOrderLinks.Flink + load_offset );
				prev_entry->InLoadOrderLinks.Flink = forward_link;
				next_entry->InLoadOrderLinks.Flink = backward_link;

				// Mem Order
				forward_link = next->InMemoryOrderLinks.Flink;
				backward_link = next->InMemoryOrderLinks.Blink;
				prev_entry = ( LDR_DATA_TABLE_ENTRY * )(  ( uintptr_t )next->InMemoryOrderLinks.Blink + mem_offset );
				next_entry = ( LDR_DATA_TABLE_ENTRY * )(  ( uintptr_t )next->InMemoryOrderLinks.Flink + mem_offset );
				prev_entry->InMemoryOrderLinks.Flink = forward_link;
				next_entry->InMemoryOrderLinks.Flink = backward_link;

				// Init Order
				forward_link = next->InInitializationOrderLinks.Flink;
				backward_link = next->InInitializationOrderLinks.Blink;
				prev_entry = ( LDR_DATA_TABLE_ENTRY * )(  ( uintptr_t )next->InInitializationOrderLinks.Blink + init_offset );
				next_entry = ( LDR_DATA_TABLE_ENTRY * )(  ( uintptr_t )next->InInitializationOrderLinks.Flink + init_offset );
				prev_entry->InInitializationOrderLinks.Flink = forward_link;
				next_entry->InInitializationOrderLinks.Flink = backward_link;

				Log( "Everything should be written back now. Good Luck!" );
				return true;
			}

			if ( next->InMemoryOrderLinks.Flink == start->InMemoryOrderLinks.Blink )
				break;
		} while ( true );

		Log( "Failed." );
		return false;
	}

	uint64_t GetModule( PEB64 * pPEB, const wchar_t * mod )
	{
		PEB_LDR_DATA64 ldr = { 0 };
		read<PEB_LDR_DATA64>( pPEB->Ldr, &ldr );
		auto cur = ldr.InMemoryOrderModuleList.Flink;
		auto start = CONTAINING_RECORD( cur, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );
		//Log( "BASE of %S AT %llx", start->FullDllName.Buffer, start->DllBase );
		LDR_DATA_TABLE_ENTRY64 next = *start;
		do
		{
			auto loc = CONTAINING_RECORD( 0, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );
			read<LDR_DATA_TABLE_ENTRY64>( ( uint64_t )loc + next.InMemoryOrderLinks.Flink, &next );
			//next = CONTAINING_RECORD( &next, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks );

			if ( wcsstr( next.FullDllName.Buffer, mod ) )
			{
				Log( "BASE of %S AT %llx", next.FullDllName.Buffer, next.DllBase );
				return next.DllBase;
			}

			if ( next.InMemoryOrderLinks.Flink == start->InMemoryOrderLinks.Blink )
				break;
		} while ( true );

		return 0;
	}

	uint64_t GetExport( uint64_t mod, const char * name )
	{
		IMAGE_DOS_HEADER pDos = { 0 };
		read<IMAGE_DOS_HEADER>( mod, &pDos );

		auto nth_pos = pDos.e_lfanew + mod;
		IMAGE_NT_HEADERS64 nth = { 0 };
		read<IMAGE_NT_HEADERS64>( nth_pos, &nth );

		auto exp = nth.OptionalHeader.DataDirectory[ IMAGE_DIRECTORY_ENTRY_EXPORT ];
		IMAGE_EXPORT_DIRECTORY ied = { 0 };
		read<IMAGE_EXPORT_DIRECTORY>( exp.VirtualAddress + mod, &ied );
		uint64_t name_addr = ied.AddressOfNames + mod;
		uint64_t func_addr = ied.AddressOfFunctions + mod;
		uint64_t ord_addr = ied.AddressOfNameOrdinals + mod;
		int i = 0;

		for ( int i = 0; i < ied.NumberOfNames; i++ )
		{
			auto cur_name = name_addr + ( i * 4 );
			uint32_t name_off = 0;
			read<uint32_t>( cur_name, &name_off );
			char name_buffer[ 256 ] = { 0 };
			read<decltype(name_buffer)>( name_off + mod, &name_buffer );


			if ( !_stricmp( name_buffer, name ) )
			{
				auto cur_ord = ord_addr + ( i * 2 );
				uint16_t ord_off = 0;
				read<uint16_t>( cur_ord, &ord_off );

				auto cur_func = func_addr + ( ord_off * 4 );
				uint32_t func_off = 0;
				read<uint32_t>( cur_func, &func_off );
				int x = 0;
				return ( func_off + mod );
			}
		}

		return 0;
	}

	void Setup64BitExceptionHandler( )
	{
		static bool bOnce = false;
		if ( !bOnce )
		{
			bOnce = true;
			auto peb = ( PEB64 * )GetPEB64( );
			Log( "PEB IS %llx", peb );
			auto nt = GetModule( peb, L"SYSTEM32\\ntdll" );
			Log( "NTDLL AT %llx", nt );
			auto exp = GetExport( nt, "RtlAddVectoredExceptionHandler" );
			Log( "RtlAddVectoredExceptionHandler at %llx", exp );
			Log( "Attempting to call x64 assembly." );
			SetupLongHandler( exp );
			Log( "Assembly executed. We havent crashed yet boss." )
		}
	}
}
#include <Windows.h>

namespace Utility {
	bool bCompare( const char * pData, const char * bMask, const char * szMask );

	uintptr_t FindPattern( uintptr_t dwAddress, uintptr_t dwLen, const char * bMask, const char * szMask );

	uintptr_t FindPattern( uintptr_t dwAddress, uintptr_t dwLen, char * pat );
}
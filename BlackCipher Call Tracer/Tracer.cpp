#include "Tracer.hpp"
#include <TlHelp32.h>
#include <algorithm>
#include <ntdll.h>
#include "HeavensGate.hpp"
#include "Utility.hpp"

#define ENABLE_LOG 1
#define LOG_UNIQUE 1
uintptr_t ntbase = 0, ntsize = 0;
uintptr_t kbase = 0, ksize = 0;

void SetByte( uintptr_t loc, unsigned char byte )
{
	DWORD old = 0;
	VirtualProtect( ( void * )loc, 1, PAGE_EXECUTE_READWRITE, &old );
	*( unsigned char * )loc = byte;
	VirtualProtect( ( void * )loc, 1, old, &old );

	//auto success = FlushInstructionCache( ( HANDLE )-1, ( void * )loc, 0 );
}

uintptr_t MatchSyscallToSymbol( int32_t syscall_index )
{
	// TODO: SigScan regular ntdll and check if we get an ID match.
	char sig[ 100 ] = { 0 };
	auto split_integer = [ syscall_index ]( int byte_number ) -> unsigned char
	{
		// assume we have the number 0x26 and the number 0x100
		// in 0x26, first byte is 26, remaining 3 are 0
		// in 0x100, first byte is 0, second is 10, remaining is 0
		auto out_byte = ( syscall_index >> ( 8 * byte_number ) ) & 0xFF;
		return out_byte;
	};

	auto one = split_integer( 0 );
	auto two = split_integer( 1 );
	auto three = split_integer( 2 );
	auto four = split_integer( 3 );

	sprintf_s( sig, "B8 %X %X %X %X BA 50 8C", one, two, three, four );
	Log( "Searching for signature %s", sig );
	uintptr_t found_pat = 0;
	found_pat = Utility::FindPattern( ntbase, ntsize, sig );
	return found_pat;
}

int32_t GetSyscallForEip( uintptr_t eip )
{
	unsigned int syscall_index = 0xDEADBABE;
	for ( int i = 0; i < 10; i++ )
	{
		auto pos = eip + i;
		if ( *( unsigned char * )pos == 0xB8 )
		{
			syscall_index = *( unsigned int * )( pos + 1 );
			break;
		}
	}

	return syscall_index;
}

void IndirectTracer::DumpRegisters( CONTEXT * pCtx )
{
	LogB( "\n" );
	LogB( "Edi = %X", pCtx->Edi );
	LogB( "Esi = %X", pCtx->Esi );
	LogB( "Ebx = %X", pCtx->Ebx );
	LogB( "Edx = %X", pCtx->Edx );
	LogB( "Ecx = %X", pCtx->Ecx );
	LogB( "Eax = %X", pCtx->Eax );
	LogB( "Ebp = %X", pCtx->Ebp );
	LogB( "Esp = %X\n", pCtx->Esp );
}

void IndirectTracer::SetDebugCtl( CONTEXT * pCtx, bool enabled )
{
	if ( enabled )
	{
		pCtx->Dr7 |= ( 1 << 9 ); // set BTF
		//pCtx->Dr7 |= ( 1 << 8 ); // set LBR flag
	}
	else
		pCtx->Dr7 = 0;
}

void IndirectTracer::SetTrapFlag( CONTEXT * pCtx, bool enabled )
{
	pCtx->EFlags = enabled ? pCtx->EFlags | 0x100 : pCtx->EFlags & ~( 0x100 );
}

void IndirectTracer::AddTraceModule( uintptr_t base, size_t size )
{
	this->trace_modules.push_back( { base, size } );
	if ( this->initialized == false )
	{
		if ( this->trace_modules.size( ) )
		{
			LogB( "Tracer now initialized." );
			this->initialized = true;
		}
	}

}

SIZE_T IndirectTracer::GetModuleSize( uintptr_t base )
{
	MEMORY_BASIC_INFORMATION region = { 0 };
	ULONG out_size = 0;
	auto stat = NtQueryVirtualMemory( ( HANDLE )-1, ( void * )base, MemoryRegionInformation, &region, sizeof( region ), &out_size );
	if ( stat != 0 )
	{
		if ( stat == 0xC0000004 )
		{
			Log( "Actual size is %d not %d", out_size, sizeof( region ) );
		}

		Log( "NTQVM ERROR: %X", stat );
		return 0;
	}

	return region.RegionSize;
}

bool IndirectTracer::HeavensGateDetected( CONTEXT * pCtx )
{
	auto eip = pCtx->Eip;
	//if ( *( uintptr_t * )( eip - 4 ) != 0 )
	//	return false;

	if ( *( uintptr_t * )eip != 0x05240483 )
		return false;

	if ( *( unsigned char * )( eip + 4 ) != 0xCB )
		return false;

	return true;
}

bool IndirectTracer::AcquireLock( )
{
	return TryAcquireSRWLockExclusive( &this->lock );
}

void IndirectTracer::ReleaseLock( )
{
	return ReleaseSRWLockExclusive( &this->lock );
}

void IndirectTracer::AddToWatch( std::initializer_list<const char *> list )
{
	for ( auto & x : list )
	{
		auto str = std::string( x );
		LogB( "Adding %s to module watch", str.c_str( ) );
		this->module_watch.push_back( str ); // returning an std::string is actually memory safe. Cool!
	}
}

void IndirectTracer::CheckWatch( )
{
	bool begin_tracing = false;
	if ( this->module_watch.size( ) )
	{
		auto cpy = this->module_watch;
		for ( auto x : cpy )
		{
			auto str = x.c_str( );
			auto mod = ( uintptr_t )GetModuleHandleA( str );
			if ( mod == 0 )
				continue;

			LogB( "%s loaded.", str );
			auto size = this->GetModuleSize( mod );
			LogB( "%s start: %p | size: %X", str, mod, size );

			if ( this->initialized == false )
				begin_tracing = true; // we have at least 1 module to trace now, start tracing!

			this->AddTraceModule( ( uintptr_t )mod, size );
			if ( x == "BlackCall.aes" )
			{
				LogB( "Setting main module." );
				module_size = size;
				module_start = mod;
			}

			this->module_watch.erase( std::find( this->module_watch.begin( ), this->module_watch.end( ), x ) );
		}
	}

	if ( begin_tracing )
	{
		LogB( "Tracer Initialized. Beginning trace." );
		this->initialized = true;
		this->StartTracing( );
	}
}

void IndirectTracer::HandleRDTSC( CONTEXT * pCtx )
{
	// A RDTSC should have just executed. Zero out EDX:EAX
	//if ( pCtx->Edx || pCtx->Eax )
	//{
	//	LogF( "Old RDTSC Values at %p: [%X:%X]", pCtx->Eip, pCtx->Edx, pCtx->Eax );
	//}

	pCtx->Edx = 0;
	pCtx->Eax = 0;

	//pCtx->Eip += 2; // skip over the rdtsc.
}

void IndirectTracer::AddBP( uintptr_t loc, CONTEXT * pCtx )
{
	pCtx->Dr0 = loc;
	pCtx->Dr7 |= ( 1 ); // enable dr0.
	//this->bps.insert( { loc, *( unsigned char * )( loc ) } );
	//SetByte( loc, 0xCC );
}

const wchar_t * IndirectTracer::GetModuleForAddress( uintptr_t addr )
{
	char buffer[ MAX_PATH * 2 ] = { 0 };
	PUNICODE_STRING us = 0;
	std::wstring big_name;
	SIZE_T ret_len = 0;
	auto stat = NtQueryVirtualMemory( ( HANDLE )-1, ( void * )addr, MemoryMappedFilenameInformation, &buffer, sizeof( buffer ), &ret_len );
	if ( stat != STATUS_SUCCESS )
	{
		//LogB( "Failed to query module name for addr %p with fail code %X", addr, stat );
	}
	else
	{
		//Log( "BUFFER AT %p", buffer );
		us = ( PUNICODE_STRING )( buffer );
		big_name = us->Buffer;
		auto fname_pos = big_name.find_last_of( L"\\" );
		big_name = big_name.substr( fname_pos + 1 );

		//LogB( "Return module for %p is %S", addr, big_name.c_str( ) );
		//MessageBoxA( 0, "CHECK ME", "HERRO", MB_ICONERROR );
	}

	return us ? big_name.c_str( ) : L"PRIVATE";
}

bool IndirectTracer::AddThread( HANDLE hThread )
{
	auto tid = GetThreadId( hThread );
	//LogB( "Got TEB for thread %d [ %p ]", tid, teb );
	uintptr_t given_addr = 0;

	//uintptr_t start_addr = 0;
	auto status = NtQueryInformationThread( ( HANDLE )-2, ThreadQuerySetWin32StartAddress, &given_addr, sizeof( given_addr ), 0 );
	if ( status != STATUS_SUCCESS )
	{
		LogB( "FAILED TO QIT %X", status );
	}

	if ( given_addr && this->AddressInModule( given_addr ) )
	{
		Log( "Added thread %d by bping entry %p", tid, given_addr );
		this->new_threads.insert( { given_addr, *( unsigned char * )given_addr } );
		// trigger an exception to be caught by our handler. Then trigger the debug breakpoints
		SetByte( given_addr, 0xCC );

		return true;
	}

	return false;
}

bool IndirectTracer::AddressInModule( uintptr_t addr, uintptr_t module_guess )
{
	for ( int i = 0; i < this->trace_modules.size( ); i++ )
	{
		auto cur_module = this->trace_modules.at( i );
		auto start = cur_module.first;
		auto size = cur_module.second;
		if ( module_guess )
		{
			if ( start != module_guess )
				continue;
		}

		auto real_start = start + 0x1000; // lol code page // TODO: make sure we're actually within a code page.

		if ( addr > real_start && addr < ( real_start + size ) )
		{
			MEMORY_BASIC_INFORMATION mbi = { 0 };
			if ( !VirtualQuery( ( void * )addr, &mbi, sizeof( mbi ) ) )
				continue;

			// make sure its an executable spot.
			if ( !( mbi.Protect & PAGE_EXECUTE ) && !( mbi.Protect & PAGE_EXECUTE_READ )
				&& !( mbi.Protect & PAGE_EXECUTE_WRITECOPY ) && !( mbi.Protect & PAGE_EXECUTE_READWRITE ) )
				continue;

			return true;
		}
	}

	return false;
}

uintptr_t IndirectTracer::WalkStackForModuleAddress( HANDLE hThread, CONTEXT ctx )
{
	STACKFRAME sf;
	sf.AddrPC.Offset = ctx.Eip;
	sf.AddrPC.Mode = AddrModeFlat;
	sf.AddrFrame.Offset = ctx.Ebp;
	sf.AddrFrame.Mode = AddrModeFlat;
	sf.AddrStack.Offset = ctx.Esp;
	sf.AddrStack.Mode = AddrModeFlat;
	// wew.
	LogF( "Trace of thread %d", GetThreadId( hThread ) );
	while ( StackWalk( IMAGE_FILE_MACHINE_I386, GetCurrentProcess( ), hThread, &sf, &ctx, 0, SymFunctionTableAccess, SymGetModuleBase, 0 ) )
	{
		LogF( "EIP: %p (%S) | RET: %p", sf.AddrPC.Offset, this->GetModuleForAddress( sf.AddrPC.Offset ), sf.AddrReturn.Offset );
		// return first one thats within our range
		if ( this->AddressInModule( sf.AddrPC.Offset ) )
			return sf.AddrPC.Offset;
	}

	return 0;
}

uintptr_t IndirectTracer::FindRealDLLForEIP( uintptr_t eip )
{

	if ( !kbase )
	{
		kbase = ( uintptr_t )GetModuleHandleA( "kernelbase.dll" );
		ksize = this->GetModuleSize( kbase );
	}

	char buf[ 50 ] = { 0 };
	memcpy( buf, ( void * )eip, sizeof( buf ) );
	char mask[ 50 ] = { 0 };
	memset( mask, 'x', sizeof( mask ) );
	auto loc = Utility::FindPattern( ntbase, ntsize, buf, mask );
	if ( loc )
		return loc;

	loc = Utility::FindPattern( kbase, ksize, buf, mask );
	return loc;
}

uintptr_t IndirectTracer::CheckRDTSC( uintptr_t eip )
{
	char buf[ ] = { 0x0F, 0x31 };
	char mask[ ] = { 'x', 'x' };
	auto loc = Utility::FindPattern( eip, 20, buf, mask );
	return loc;
}

void IndirectTracer::BranchCallback( uintptr_t addr, CONTEXT * pCtx )
{
	auto _addr = ( void * )addr;
	auto get_parameter = [ pCtx ]( int argument = 1 ) -> uintptr_t
	{
		return *( uintptr_t * )( pCtx->Esp + ( argument * sizeof( void * ) ) );
	};

	auto get_return = [ pCtx ]( ) -> uintptr_t
	{
		return *( uintptr_t * )( pCtx->Esp );
	};

	auto skip_call = [ pCtx, get_return ]( uintptr_t return_value )->void
	{
		pCtx->Eip = get_return( );
		pCtx->Eax = return_value;
	};
	if ( !kbase )
	{
		kbase = ( uintptr_t )GetModuleHandleA( "kernelbase.dll" );
		ksize = this->GetModuleSize( kbase );
	}
	auto quick_kbase_gpa = [ ]( const char * name )
	{
		return GetProcAddress( ( HMODULE )kbase, name );
	};

	if ( _addr == LoadLibraryA || _addr == LoadLibraryExA )
	{
		LogB( "[LLA] Loading library [%s]", ( char * )get_parameter( ) );
	}
	else if ( _addr == LoadLibraryW || _addr == LoadLibraryExW )
	{
		LogB( "[LLW] Loading library [%S]", ( wchar_t * )get_parameter( ) );
	}
	else if ( _addr == CreateFileA )
	{
		LogB( "[CreateFileA] Creating File [%s] | Disposition: %d | Access: 0x%X", ( char * )get_parameter( ), get_parameter( 5 ), get_parameter( 2 ) );
	}
	else if ( _addr == CreateFileW )
	{
		LogB( "[CreateFileW] Creating File [%S] | Disposition: %d | Access: 0x%X", ( wchar_t * )get_parameter( ), get_parameter( 5 ), get_parameter( 2 ) );
	}
	else if ( _addr == GetModuleHandleExW )
	{
		LogB( "[GMHWX] Looking for module [%S]", ( wchar_t * )get_parameter( 2 ) );
	}
	else if ( _addr == GetModuleHandleW )
	{
		LogB( "[GMHW] Looking for module [%S]", ( wchar_t * )get_parameter( ) );
	}
	else if ( _addr == GetProcAddress )
	{
		auto mod = ( HMODULE )get_parameter( );
		auto api = ( char * )get_parameter( 2 );
		LogB( "[GPA] Looking for API [%s | %p]", api, GetProcAddress( mod, api ) );
	}
	else if ( _addr == VirtualProtect )
	{
		auto loc = get_parameter( );
		auto size = ( size_t )get_parameter( 2 );
		auto protect = ( DWORD )get_parameter( 3 );

		LogB( "[VProtect] Protecting memory: [ %p | Protect: %X | %d bytes ]", loc, protect, size );
	}
	else if ( _addr == CreateThread )
	{
		auto start_thread = get_parameter( 3 );
		auto thread_ctx = get_parameter( 4 );

		LogB( "[CT] Creating thread at %p with parameter %p", start_thread, thread_ctx );

	}
	else if ( _addr == GetWindowTextA )
	{
		auto hwnd = ( HWND )get_parameter( );
		char buff[ 256 ] = { 0 };
		GetWindowTextA( hwnd, buff, sizeof( buff ) );
		LogB( "Called GetWindowTextA on window %s", buff );
	}
	else if ( _addr == NtQuerySystemInformation )
	{
		auto info_class = ( int )get_parameter( );
		LogB( "[QuerySystemInfo] Querying system information for class %d", info_class );
	}
	else if ( _addr == NtQueryInformationProcess )
	{
		auto proc = ( HANDLE )get_parameter( );
		auto info_class = ( int )get_parameter( 2 );
		LogB( "[QueryProcessInfo] Querying process %d for class %d", GetProcessId( proc ), info_class );
	}
	else if ( _addr == NtSetInformationThread )
	{
		auto thread = ( HANDLE )get_parameter( );
		auto tic = ( int )get_parameter( 2 );
		LogB( "[SetThreadInfo] Setting info class of %d on thread id %d", tic, GetThreadId( thread ) );
	}
	else if ( _addr == ReadFile )
	{

	}
	else if ( _addr == FindWindowA )
	{
		auto classname = ( char * )get_parameter( );
		auto windowname = ( char * )get_parameter( 2 );
		Log( "[FindWindow] Searching for [ %s | %s ]", classname, windowname );
		// oh baby! In maplestory this includes a lot more than ollydbg! Also Regmon, filemon and procmon!
		skip_call( 0 );
	}
	else if ( _addr == CreateFileMappingA )
	{
		auto file = get_parameter( );
		auto size = get_parameter( 5 );
		auto mapping_name = ( char * )get_parameter( 6 );
		LogB( "Creating a file mapping with name %s from handle %X with size %X (%d)", mapping_name, file, size, size );
	}
	else if ( _addr == DeviceIoControl )
	{
		auto device = get_parameter( );
		auto code = get_parameter( 2 );

		LogB( "Calling DeviceIoControl on device %X with code %X", device, code );
	}
	else if ( _addr == IsDebuggerPresent )
	{
		//LogB( "[Debugger] Hid Debugger Present call" );
		skip_call( 0 );
	}
	else if ( _addr == CreateToolhelp32Snapshot )
	{
		auto snap = get_parameter( 2 );
		LogB( "[TH32] Creating Snapshot for %d", snap );
	}
	else if ( _addr == Module32Next )
	{
		auto snap = ( HANDLE )get_parameter( );
		auto ptr = ( MODULEENTRY32 * )get_parameter( 2 );
		auto ret = Module32Next( snap, ptr );

		if ( !_stricmp( ptr->szModule, "BlackCipher Call Tracer.dll" ) )
		{
			LogB( "Found blackcipher tracer! Skipping it!" );
			ret = Module32Next( snap, ptr ); // skip me!
			skip_call( ret );
		}

		skip_call( 0 ); // did it for them!
	}
	else if ( _addr == quick_kbase_gpa( "GetTickCount" ) )
	{
		LogB( "GetTickCount called. Returning 0!" );
		skip_call( 0 );
	}
	else if ( _addr == quick_kbase_gpa( "GetSystemTimeAsFileTime" ) )
	{
		LogB( "Calling GetSystemTimeAsFileTime" );
	}
}

void IndirectTracer::HandleSingleStep( EXCEPTION_POINTERS * pPtrs )
{
	auto cr = pPtrs->ContextRecord;
	auto er = pPtrs->ExceptionRecord;

	auto byte_opcode = *( unsigned char * )cr->Eip;
	if ( this->HeavensGateDetected( cr ) )
	{
#pragma region Heavens Gate stuff
		auto mb = SymGetModuleBase( this->sym_handle, cr->Eip );
		LogB( "We are hitting heavens gate at %p (%p)", cr->Eip, cr->Eip - mb );
		// forcibly set cs and all that?
		// no. windows doesnt allow you to switch modes via setthreadcontext, smart tbh.
		// so, what if we set a breakpoint instead, and disable our single stepping?
		pTracer->SetDebugCtl( cr, false );
		pTracer->SetTrapFlag( cr, false );
		HeavensGate::Setup64BitExceptionHandler( );
		this->AddBP( cr->Eip + 5, cr );
		this->dirty = true;
		return;
#pragma endregion
	}
	else if ( this->AddressInModule( cr->Eip ) )
	{
#pragma region logging stuff

		if ( std::find( this->seen_addrs.begin( ), this->seen_addrs.end( ), cr->Eip ) == this->seen_addrs.end( ) )
		{
			this->seen_addrs.push_back( cr->Eip ); // push back once, write to log once.
			auto mod_base = SymGetModuleBase( this->sym_handle, cr->Eip );
			wchar_t mod_name[ 250 ] = { 0 };
			wcscpy_s( mod_name, this->GetModuleForAddress( cr->Eip ) );
			//Log( "In %S at %p (%p) [%X]", mod_name, cr->Eip, mod_base ? cr->Eip - mod_base : 0, byte_opcode );
		}

#pragma endregion

		auto rdtsc = this->CheckRDTSC( cr->Eip );
		if ( rdtsc )
		{
			//LogB( "Potential RDTSC detected at %p. (EIP: %p)", rdtsc, cr->Eip );
			this->AddBP( rdtsc + 2, cr ); // leave debugctl and trap flag in case we dont hit it.. + 2 because we want the instruction after rdtsc.
			//LogB( "BP set at %p", rdtsc + 2 );
		}

		return;
	}
	else
	{
#pragma region Symbol Identification
		// Not in module? Get symbol name, and call our indirect callback
		auto call_from = *( uintptr_t * )cr->Esp;
		bool ret_in_module = this->AddressInModule( call_from );

		wchar_t mod_name[ 250 ] = { 0 };
		wcscpy_s( mod_name, this->GetModuleForAddress( call_from ) );
		wchar_t eip_name[ 250 ] = { 0 };
		wcscpy_s( eip_name, this->GetModuleForAddress( cr->Eip ) );
		auto private_memory = !_wcsicmp( mod_name, L"PRIVATE" );
		uintptr_t fake_call = 0;

		if ( std::find( this->seen_fns.begin( ), this->seen_fns.end( ), cr->Eip ) == this->seen_fns.end( ) )
		{
			this->seen_fns.push_back( cr->Eip ); // push back once, write to log once.

			DWORD64 disp = 0;
			char buf[ sizeof( SYMBOL_INFO ) + 257 ] = { 0 };
			SYMBOL_INFO * pInfo = ( SYMBOL_INFO * )buf;
			pInfo->SizeOfStruct = sizeof( SYMBOL_INFO );
			pInfo->MaxNameLen = 256;

			auto syscall = GetSyscallForEip( cr->Eip );
			if ( syscall != 0xDEADBABE )
			{
				// we got a syscall!
				auto sym = MatchSyscallToSymbol( syscall );
				if ( SymFromAddr( this->sym_handle, sym, &disp, pInfo ) )
				{
					if ( disp == 0 )
					{
						auto ret_start = SymGetModuleBase( this->sym_handle, call_from );
						LogB( "Indirect Syscall (%X): %s [%S EIP: %p | RET: %S - %p (relative: %d)]", syscall, pInfo->Name, eip_name, cr->Eip, mod_name, call_from - ret_start, ret_start ? 1 : 0 );
					}
				}
			}
			else
			{
				if ( private_memory || wcsstr( eip_name, L"tmp" ) )
				{
					// potential fake call
					fake_call = this->FindRealDLLForEIP( cr->Eip );
					if ( fake_call )
					{
						wchar_t real_mod[ 250 ] = { 0 };
						wcscpy_s( real_mod, this->GetModuleForAddress( fake_call ) );
						LogB( "FAKE CALL. REAL ADDR: %p | MODULE: %S ", fake_call, real_mod );
					}
				}

				if ( SymFromAddr( this->sym_handle, fake_call ? fake_call : cr->Eip, &disp, pInfo ) )
				{
					if ( disp == 0 )
					{
						auto ret_start = SymGetModuleBase( this->sym_handle, call_from );
						LogB( "Symbol: %s [%S EIP: %p | RET: %S - %p (relative: %d)]", pInfo->Name, eip_name, cr->Eip, mod_name, call_from - ret_start, ret_start ? 1 : 0 );
					}
				}
				else
				{
					if ( byte_opcode != 0xFF ) // not a JMP DWORD
					{
						if ( !_wcsicmp( eip_name, L"wow64cpu.dll" ) )
						{
							LogB( "IN WOW64CPU, DEF SYSCALLING. DUMPING." );
							DumpRegisters( cr );
						}
						else
						{
							LogB( "Failed to get symbol info [%S EIP: %p | %S - RET: %p]", eip_name, cr->Eip, mod_name, call_from );
						}
					}
				}
			}
		}
#pragma endregion

		this->BranchCallback( fake_call ? fake_call : cr->Eip, cr );

		if ( ret_in_module || private_memory ) // this may not be the 
		{
			if ( byte_opcode == 0xFF && *( unsigned char * )( cr->Eip + 1 ) == 0x25 ) // JMP DWORD (probably to an API)
				return;

			this->SetDebugCtl( cr, false );
			this->SetTrapFlag( cr, false );
			this->AddBP( call_from, cr );
			//LogB( "DR0 && DR7 == %p | %X", cr->Dr0, cr->Dr7 );
		}
		else
		{
			// Perhaps this thread has finished its job.
			LogB( "Is this ever even hit? Lol." );
			this->SetDebugCtl( cr, false );
			this->SetTrapFlag( cr, false );
		}
	}
}

bool IndirectTracer::WalkThreadsToBP( )
{
	long me = GetCurrentThreadId( );
	bool success = true; // lol

	int count = 0;

	THREADENTRY32 te = {};
	te.dwSize = sizeof( te );
	HANDLE hSnap = CreateToolhelp32Snapshot( TH32CS_SNAPTHREAD, GetCurrentProcessId( ) );
	if ( Thread32First( hSnap, &te ) )
	{
		do
		{
			if ( te.th32OwnerProcessID == GetCurrentProcessId( ) && te.th32ThreadID != me )
			{
				auto tid = te.th32ThreadID;
				HANDLE hThread = OpenThread( THREAD_SET_CONTEXT | THREAD_GET_CONTEXT | THREAD_SUSPEND_RESUME | THREAD_QUERY_INFORMATION, false, tid );
				if ( !hThread || hThread == ( HANDLE )-1 )
				{
					LogB( ":(" ); // ignoring errors cause lol
					success = false;
					break;
				}

				if ( SuspendThread( hThread ) == ( DWORD )-1 )
				{
					LogB( "!!!" );
					success = false;
					break;
				}

				CONTEXT ctx = { 0 };
				ctx.ContextFlags = CONTEXT_CONTROL | CONTEXT_DEBUG_REGISTERS;

				if ( !GetThreadContext( hThread, &ctx ) )
				{
					Log( "Failed to get context." );
					success = false;
					break;
				}

				if ( this->AddressInModule( ctx.Eip ) )
				{
					LogB( "EIP is within module for thread %d [%p]", GetThreadId( hThread ), ctx.Eip );

					pTracer->AddBP( ctx.Eip, &ctx );
					count++;
				}
				else
				{
					// should we instead walk the thread stack and find threads only with our module in the call stack, and breakpoint there instead of randomly where its frozen (ntdll 100% of the time)
					// ThreadBasicInformation -> TEB -> stack
					// set trap flag boiiiiiiiiiii

						//auto given_addr = WalkStackForModuleAddress( ( TEB * )teb );
					auto given_addr = WalkStackForModuleAddress( hThread, ctx );

					if ( given_addr )
					{
						LogB( "Breakpointing module address %p for thread %d", given_addr, tid );
						//SetByte( given_addr, 0xCC );
						pTracer->AddBP( given_addr, &ctx );
						count++;
					}
					else
					{
						auto status = NtQueryInformationThread( hThread, ThreadQuerySetWin32StartAddress, &given_addr, sizeof( given_addr ), 0 );
						if ( status != STATUS_SUCCESS )
						{
							LogB( "FAILED TO QIT %X", status );
						}

						if ( given_addr && this->AddressInModule( given_addr ) )
						{
							//LogB( "Thread start address is %p." );
							pTracer->AddBP( given_addr, &ctx );
							count++;
						}
						//LogB( "Main module not in thread %d's stack", GetThreadId( hThread ) );
					}
				}

				if ( !SetThreadContext( hThread, &ctx ) )
				{
					Log( "Failed to set context." );
					success = false;
					break;
				}

				ResumeThread( hThread );
				//break; // TODO: remove this break.
			}

		} while ( Thread32Next( hSnap, &te ) );
	}

	CloseHandle( hSnap );
	LogB( "We breakpointed %d threads", count );
	return success;
}

bool IndirectTracer::StartTracing( )
{
	if ( !this->initialized )
	{
		LogB( "Not Initialized" );
		return false;
	}

	// do i need to loop through every thread? :( 
	this->active = true;
	if ( this->WalkThreadsToBP( ) )
	{
		LogB( "Successfully set SingleStep exceptions on branch change\n" );
		return true;
	}
	else
	{
		LogB( "We're bad and we should feel bad." );
		return false;
	}
}

// Not a class function because its program-specific.
extern "C" void __cdecl SetupLongHandler( uint64_t RtlExceptionHandlerFn );

void DumpSyscallStruct( HeavensGate::CONTEXT64 * pStruct )
{
	LogB( "RCX = %llx", pStruct->Rcx );
	LogB( "RDX = %llx", pStruct->Rdx );
	LogB( "R8 = %llx", pStruct->R8 );
	LogB( "R9 = %llx\n", pStruct->R9 );
}

void SyscallCallback32( int syscall_index, CONTEXT pCtx )
{

}

void SyscallCallback64( int syscall_index, HeavensGate::CONTEXT64 * pCtx )
{
	if ( syscall_index == 0x26 ) // If i want to be able to catch and modify these on the fly, we would need to return to 32 bit mode and attempt to modify these structures.
	{
		LogB( "[ZwOpenProcess] %llx == ClientId", pCtx->R9 );
		auto cid = ( uint64_t * )pCtx->R9;
		DumpSyscallStruct( pCtx );
		LogB( "PID: %d", (DWORD)cid[ 0 ] );

		//MessageBoxA( 0, "BLEEP", "FOOK", 0 );
	}
	else if ( syscall_index == 0x23 )
	{
		auto hHandle = ( HANDLE )pCtx->Rcx;
		auto mod_name = pTracer->GetModuleForAddress( pCtx->Rdx );
		LogB( "[NtQueryVirtualMemory] PID: %d | Class: %d | Loc: %llx [%S]", GetProcessId( hHandle ), ( int )pCtx->R8, pCtx->Rdx, mod_name );
	}
	else if ( syscall_index == 0x3F )
	{
		auto handle = ( HANDLE )pCtx->Rcx;
		auto loc = pCtx->Rdx;
		auto pid = GetProcessId( handle );
		auto mod_name = pTracer->GetModuleForAddress( loc );
		LogB( "[ZwReadVirtualMemory] process %d at location %llx which is module %S", pid, loc, mod_name );
	}
	else if ( syscall_index == 0x19 )
	{
		auto handle = ( HANDLE )pCtx->Rcx;
		auto info = ( long )pCtx->Rdx;
		LogB( "[ZwQueryInformationProcess] PID: %d | Class: %d", GetProcessId( handle ), ( long )pCtx->Rdx );
		if ( info == ProcessBasicInformation )
		{
			auto peb = ( HeavensGate::PEB64 * )HeavensGate::GetPEB64( );
			// walk PEB and remove our module.
		}
	}
	else
	{
		LogB( "Dump struct for syscall %X", syscall_index );
		DumpSyscallStruct( pCtx );
	}
}

extern "C" void HeavensBridge( HeavensGate::CONTEXT64 * pCtx )
{
	/*
		I CAN SEE CLEARLY NOW THE RAIN IS GONE.
		So, eventually we want to KiFastSystemCall. This does a jmp [r15+0xF8], it so happens r15 was SUPER SCUFFED.
		so, we can replace r15 with the one we have in our CONTEXT, and all is good in the world.
		So we fix this, and then we get stuck attempting to acquire our spinlock. Weeeeeird.

		Followup, we also needed to reset r14, as r14 is exchanged with esp on fast entry
	*/

	//LogB( "I HAVE BEEN CALLED FROM BEYOND THE GATE" );
	LogB( "Context structure at %p, eip : %llx", pCtx, pCtx->Rip );
	// symbol check will never work. Assume its a syscall.
	// perhaps its a manual syscall. check for a 0xB8, then get the ID.
	auto loc = ( uintptr_t )pCtx->Rip;
	LogB( "Checking %p", loc );
	auto mod_name = pTracer->GetModuleForAddress( ( uintptr_t )pCtx->Rip );
	if ( mod_name )
	{
		LogB( "In module: %S", mod_name );
	}

	auto syscall_index = GetSyscallForEip( loc );
	if ( syscall_index != 0xDEADBABE )
	{
		LogB( "Checking syscall api %X", syscall_index );
		if ( syscall_index == 0x19 )
		{
			LogB( "Symbol is ZwQueryInformationProcess" );
			//continue; // this one is unique because its literally the ONLY one that the symbol fails for.
		}
		else
		{
			auto symbol_addr = MatchSyscallToSymbol( syscall_index );
			if ( symbol_addr )
			{
				DWORD64 disp = 0;
				char buf[ sizeof( SYMBOL_INFO ) + 257 ] = { 0 };
				SYMBOL_INFO * pInfo = ( SYMBOL_INFO * )buf;
				pInfo->SizeOfStruct = sizeof( SYMBOL_INFO );
				pInfo->MaxNameLen = 256;
				if ( SymFromAddr( pTracer->sym_handle, symbol_addr, &disp, pInfo ) )
				{
					if ( disp == 0 )
					{
						LogB( "Symbol for syscall %X is : %s", syscall_index, pInfo->Name );
					}
				}
				else
				{
					LogB( "Failed to get symbol for syscall %X (found at %p)", syscall_index, symbol_addr );
				}
			}
		}

		SyscallCallback64( syscall_index, pCtx );
	}
	else
	{
		LogB( "Failed to get symbol for location %llx", pCtx->Rip );
	}
}

EXP long __stdcall BasicHandler( EXCEPTION_POINTERS * pPtrs )
{
	if ( pTracer->active )
	{
		auto cr = pPtrs->ContextRecord;
		auto er = pPtrs->ExceptionRecord;

		pTracer->SetDebugCtl( cr, true );
		pTracer->SetTrapFlag( cr, true );

		if ( er->ExceptionCode != EXCEPTION_SINGLE_STEP )
		{
			if ( er->ExceptionCode == EXCEPTION_BREAKPOINT )
			{
				auto tid = GetCurrentThreadId( );
				auto match = pTracer->new_threads.find( cr->Eip );
				if ( match != pTracer->new_threads.end( ) )
				{
					auto old_byte = match->second;
					LogB( "0xCC Hit." );
					SetByte( cr->Eip, old_byte );
					pTracer->new_threads.erase( match );
					return EXCEPTION_CONTINUE_EXECUTION;
				}
				else
				{
					//Log( "Passing on breakpoint at %p", cr->Eip );
					//cr->Eip++;
					//return EXCEPTION_CONTINUE_EXECUTION;
					return EXCEPTION_CONTINUE_SEARCH;
				}
			}
			else
			{
				auto mb = SymGetModuleBase( pTracer->sym_handle, cr->Eip );
				if ( er->ExceptionCode == EXCEPTION_ACCESS_VIOLATION )
				{
					auto mod_name = pTracer->GetModuleForAddress( cr->Eip );
					LogB( "AVIO DUMP in %S: %p (%p):", mod_name, cr->Eip, cr->Eip - mb );
					pTracer->DumpRegisters( cr );
					//MessageBoxA( 0, "AVIO HERE", "HEYYY", MB_ICONERROR );
				}
				else if ( er->ExceptionCode == 0xE06D7363 )
				{
					return EXCEPTION_CONTINUE_SEARCH; // this ones just annoying.
				}
				else if ( er->ExceptionCode == 0x406D1388 )
				{
					auto tName = ( char * )er->ExceptionInformation[ 1 ];
					LogB( "Naming thread. Chosen name: %s", tName )
				}
				//else if ( er->ExceptionCode == EXCEPTION_GUARD_PAGE )
				//{
				//	LogB( "PAGE GUARD HIT at %p!", cr->Eip );
				//	MessageBoxA( 0, "GOD BLESS", "BLESS GOD", 0 );
				//}
				else
				{
					LogB( "Got an exception of type %X in module %S [%p (%p)]. Continuing", er->ExceptionCode, pTracer->GetModuleForAddress( cr->Eip ), cr->Eip, cr->Eip - mb );
				}

				return EXCEPTION_CONTINUE_SEARCH;
			}
		}
		else
		{
			// HWBPs only trigger single steps.
			if ( cr->Eip == cr->Dr0 )
			{
				auto loc = pTracer->CheckRDTSC( cr->Eip - 2 );
				if ( loc == ( cr->Eip - 2 ) )
				{
					//LogB( "RDTSC Hit at %p - 2!", cr->Eip );
					pTracer->HandleRDTSC( cr );
					cr->Dr0 = 0;
					cr->Dr7 &= ~( 1 );
					return EXCEPTION_CONTINUE_EXECUTION;
				}

				// we hit a debug exception.
				cr->Dr0 = 0;
				cr->Dr7 &= ~( 1 );
				//LogB( "Hit breakpoint at %p", cr->Eip );
			}

			if ( pTracer->inactivated )
				pTracer->active = false;
			else
				pTracer->HandleSingleStep( pPtrs );

			return EXCEPTION_CONTINUE_EXECUTION;
		}
	}

	return EXCEPTION_CONTINUE_SEARCH;
}

// TODO: Figure out if the tracer is causing the game to crash by bad code, or if the game closes itself due to being traced.
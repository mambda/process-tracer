#include "generic_header.hpp"
#include <map>
#include <vector>
#include <DbgHelp.h>
#pragma comment(lib, "dbghelp.lib")
#define EXP extern "C"

EXP long __stdcall BasicHandler( EXCEPTION_POINTERS * pPtrs );
EXP long ASM_HANDLER( EXCEPTION_POINTERS * pPtrs );

class IndirectTracer
{
	std::vector< std::pair<uintptr_t, size_t> > trace_modules;
	std::vector<std::string> module_watch;
	SRWLOCK lock;
	std::vector<uintptr_t> seen_addrs;
	std::vector<uintptr_t> seen_fns;
	HANDLE conhandle = 0;

	bool AddressInModule( uintptr_t addr, uintptr_t module_guess = 0 ); // module_guess = base address of a module you believe this address to be in ( so we dont get triggered by some random stack shit)
	// used to know whether we're in a module currently, duh
	void BranchCallback( uintptr_t addr, CONTEXT * pCtx );
	uintptr_t WalkStackForModuleAddress( HANDLE hThread, CONTEXT ctx );
	uintptr_t FindRealDLLForEIP( uintptr_t eip ); // given an EIP, see if its current address matches ntdll, or kernelbase.dll to get the real symbol.

public:
	bool initialized = false;
	HANDLE sym_handle = nullptr;
	bool active = false;
	bool inactivated = false;
	std::map<uintptr_t, unsigned char> bps;
	std::map<uintptr_t, unsigned char> new_threads; // threadid, startaddr
	bool dirty = false; // Enabled if we have entered heavens gate

	IndirectTracer( )
	{
		InitializeSRWLock( &this->lock );
		auto handler = AddVectoredExceptionHandler( TRUE, BasicHandler );
		if ( handler == NULL )
		{
			Log( "Failed to add vectored handler" );
			Log( "Error: %X", GetLastError( ) );
		}
		else
		{
			SymSetOptions( SYMOPT_DEFERRED_LOADS | SYMOPT_UNDNAME ); // dont load EVERY module symbols, only when i ask!
			this->sym_handle = GetCurrentProcess( );
			auto suc = SymInitialize( this->sym_handle, 0, TRUE );
			if ( suc == FALSE )
			{
				LogB( "Sym initialize failed with %d", GetLastError( ) );
			}
		}

		LogB( "Constructor finished but class not initialized. Need a trace module!" );
	}

	IndirectTracer( uintptr_t base, long size )
	{
		// create our VEH handler, do not yet trigger single stepperinos.
		//VECTORED_EXC
		InitializeSRWLock( &this->lock );
		auto handler = AddVectoredExceptionHandler( TRUE, BasicHandler );
		if ( handler == NULL )
		{
			Log( "Failed to add vectored handler" );
			Log( "Error: %X", GetLastError( ) );
		}
		else
		{
			SymSetOptions( SYMOPT_DEFERRED_LOADS | SYMOPT_UNDNAME ); // dont load EVERY module symbols, only when i ask!
			this->sym_handle = GetCurrentProcess( );
			auto suc = SymInitialize( this->sym_handle, 0, TRUE );
			if ( suc == FALSE )
			{
				LogB( "Sym initialize failed with %d", GetLastError( ) );
			}

			if ( base && size )
			{
				this->trace_modules.push_back( { base, size } );
				initialized = true;
			}
		}
	}

	// Loop every thread + enable Single Step exceptions
	bool WalkThreadsToBP( );
	bool StartTracing( );

	void HandleSingleStep( EXCEPTION_POINTERS * pPtrs );
	bool AddThread( HANDLE hThread );

	const wchar_t * GetModuleForAddress( uintptr_t addr );
	void AddBP( uintptr_t loc, CONTEXT * pCtx );
	void DumpRegisters( CONTEXT * pCtx );
	void SetDebugCtl( CONTEXT * pCtx, bool enabled );
	void SetTrapFlag( CONTEXT * pCtx, bool enabled );
	void AddTraceModule( uintptr_t base, size_t size );
	SIZE_T GetModuleSize( uintptr_t base );
	bool HeavensGateDetected( CONTEXT * pCtx ); // detects if we're about to hit a heavens gate.

	bool AcquireLock( );
	void ReleaseLock( );

	void AddToWatch( std::initializer_list<const char *> list );
	void CheckWatch( ); // check if one of the watched modules is loaded. if so, do yo thang.

	uintptr_t CheckRDTSC( uintptr_t eip ); // Check if within ~20 bytes of EIP we see a RDTSC.
	void HandleRDTSC( CONTEXT * pCtx );
};

// TODO: Create a AddModuleToWatch function, which is supplied an array of strings, and checks for these modules to be loaded (check in new thread) and adds them to the watch list.

extern IndirectTracer * pTracer;

extern uintptr_t ntbase, ntsize;
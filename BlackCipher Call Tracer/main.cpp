﻿#include "Tracer.hpp"
#include <ntdll.h>
#include <algorithm>

/*
	________:00619C4A                                   GPA_CALLED_HERE_2:                      ; CODE XREF: ________:0061823F↑j
	________:00619C4A FF 95 45 39 3B 11                                 call    dword ptr [ebp+113B3945h] // GPA called here.
	________:00619C50 89 85 C6 18 3B 11                                 mov     [ebp+113B18C6h], eax
	________:00619C56 8D 85 B8 BF 5B 11                                 lea     eax, [ebp+115BBFB8h]
	________:00619C5C 89 85 89 31 3B 11                                 mov     [ebp+113B3189h], eax
	________:00619C62 FF 95 C6 18 3B 11                                 call    dword ptr [ebp+113B18C6h] // Other API called here.

	GOAL: Using single steps we walk all these calls until the program closes (or until we close it in the event we inject ourselves)
		Then: once we step into a module that is NOT blackcipher.aes/exe, we log the module and current address for future lookup.
			Potentially: Automatically get the API name probably via dbghelp
*/

/*
	Needs: VEH handler, stores events ( only indirect calls / jmps ) and writes them after buffer is full (maybe 256 entries?)
		Supplementary: parameter logging
*/


IndirectTracer * pTracer = nullptr;
FILE * pFile = nullptr;

// TODO: hotkey to toggle tracing.


bool bcall = false;

#include "HeavensGate.hpp"
BOOL WINAPI DllMain( HMODULE hMe, DWORD dw, LPVOID )
{
	if ( dw == DLL_PROCESS_ATTACH )
	{
		//DisableThreadLibraryCalls( hMe );
		AllocConsole( );
		freopen_s( &pFile, "CONOUT$", "w", stdout );
		SetConsoleTitleA( "Debug Console." );
		LogB( "PID IS %d", GetCurrentProcessId( ) );
		if ( !ntbase )
		{
			ntbase = ( uintptr_t )GetModuleHandleA( "ntdll" );
			Log( "NTDLL (x86) at %p", ntbase );
			MEMORY_BASIC_INFORMATION region = { 0 };
			ULONG out_size = 0;
			auto stat = NtQueryVirtualMemory( ( HANDLE )-1, ( PVOID )ntbase, MemoryRegionInformation, &region, sizeof( region ), &out_size );
			if ( stat != 0 )
			{
				if ( stat == 0xC0000004 )
				{
					Log( "Actual size is %d not %d", out_size, sizeof( region ) );
				}

				Log( "NT ERROR: %X", stat );
			}
			ntsize = region.RegionSize;
		}
		//HeavensGate::RemoveFromPEB( ( uintptr_t )hMe );

		auto trace_module = ( uintptr_t )GetModuleHandleA( 0 );
		char MOD_NAME[ 250 ] = { 0 };
		pTracer = new IndirectTracer( );

		GetModuleFileNameA( ( HMODULE )trace_module, MOD_NAME, sizeof( MOD_NAME ) );
		std::string mod( MOD_NAME );
		std::transform( mod.begin( ), mod.end( ), mod.begin( ), tolower );
		LogB( "Transformed name is %s", mod.c_str( ) );

		if ( strstr( mod.c_str( ), "maplestory.exe" ) )
		{
			bcall = true;
			// BUST: "jypc.dll"  "nexon_api.dll", "NexonAnalytics32.dll", "nmconew.dll",
			//auto watch_list = { "BlackCall.aes", "NGClient.aes", "NexonAnalytics32.dll",/* "nexon.dll",*/ "jypc.dll", "nmconew.dll" };
			auto watch_list = { "BlackCall.aes", "NGClient.aes", "jypc.dll", "maplestory.exe"};

			pTracer->AddToWatch( watch_list );
		}
		else
		{
			trace_module = ( uintptr_t )GetModuleHandleA( 0 );
			auto size = pTracer->GetModuleSize( trace_module );
			pTracer->AddTraceModule( trace_module, size );
			module_start = trace_module;
			module_size = size;
		}

		if ( pTracer->initialized )
			pTracer->StartTracing( );
		else
			pTracer->CheckWatch( );
	}
	else if ( dw == DLL_THREAD_ATTACH )
	{
		// check if a module we want has loaded.
		pTracer->CheckWatch( );

		if ( pTracer->active )
		{
			auto hThread = GetCurrentThread( );
			//LogB( "Thread spawned." );
			pTracer->AddThread( hThread );
		}
	}

	return TRUE;
}

// TODO: Update x64 code to loop through module list so it works for multiple modules.
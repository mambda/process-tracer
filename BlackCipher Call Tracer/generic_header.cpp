#include "generic_header.hpp"
#include <fstream>
#include <ctime>
#include <sstream>
#include <algorithm>

char * GetTime( )
{
	auto now = std::time( 0 );
	std::tm out = { 0 };
	auto local_now = localtime_s( &out, &now );

	char * out_str = new char[ 100 ]( );
	sprintf_s( out_str, 100, "%02d:%02d:%02d", out.tm_hour, out.tm_min, out.tm_sec );
	return out_str;
}

void LogToFile( const char * format, ... )
{
	//static bool bFirst = false;
	static std::ofstream * pFile = nullptr;
	if ( pFile == false )
	{
		char file_name[ 256 ] = { 0 };
		if ( GetModuleFileNameA( GetModuleHandleA( 0 ), file_name, sizeof( file_name ) ) == 0 )
		{
			Log( "COULDNT GET FILE NAME, U WOT" );
		}
		else
		{
			std::string fname( file_name );
			std::stringstream ss;
			auto name_spot = fname.find_last_of( "\\" );
			auto real_name = fname.substr( name_spot + 1 );
			name_spot = real_name.find_last_of( "." );
			real_name = real_name.substr( 0, name_spot );

			ss << "DEBUG_LOG_" << real_name << ".log";

			Log( "Creating file %s", ss.str( ).c_str( ) );

			pFile = new std::ofstream( ss.str( ).c_str( ), std::ios::trunc | std::ofstream::out );
		}
	}

	va_list vl;
	va_start( vl, format );
	char out_string[ 250 ] = { 0 };

	vsnprintf_s( out_string, sizeof( out_string ), format, vl );

	va_end( vl );

	( *pFile ) << out_string;
	pFile->flush( );

}

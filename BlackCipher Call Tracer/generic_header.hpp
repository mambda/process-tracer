#include <Windows.h>
#include <iostream>

char * GetTime( );

void LogToFile( const char * format, ... );

#define Log(format, ...) {\
	auto ptr = GetTime();\
	printf_s("[ %-20s | %d | %s ] "##format##"\n", __FUNCTION__, GetCurrentThreadId(), ptr , __VA_ARGS__);\
	delete ptr;}

#define LogF(format, ...) {\
auto ptr = GetTime();\
LogToFile("[ %-20s | %d | %s ] "##format##"\n", __FUNCTION__, GetCurrentThreadId(), ptr, __VA_ARGS__);\
delete ptr;}

#define LogB(format, ...) Log(format, __VA_ARGS__) LogF(format, __VA_ARGS__)

extern "C" uintptr_t module_start, module_size;

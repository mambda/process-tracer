#include <Windows.h>
#include <iostream>
#include <ntdll.h>

constexpr auto BC_PATH = "E:\\Games\\Other\\maplestory\\appdata\\BlackCipher\\BlackCipher.exe";

uintptr_t GetEntryPoint( HANDLE hProc )
{
	SECTION_IMAGE_INFORMATION sii = { 0 };
	DWORD ret = 0;
	printf_s( "NTQIP at %p", NtQueryInformationProcess );
	auto stat = NtQueryInformationProcess( hProc, ProcessImageInformation, &sii, sizeof( sii ), &ret );
	if ( stat != STATUS_SUCCESS )
	{
		printf_s( "NTQIP Failed with %X\n", stat );
		return 0;
	}

	printf_s( "Module entry is at %p\n", sii.TransferAddress );

	return ( uintptr_t )sii.TransferAddress;
}

uintptr_t WriteShellcode( HANDLE hProc, uintptr_t entrypoint )
{
	// suspend self thread, we resume it once injected and we will be at entrypoint!
	unsigned char suspend_shellcode[ 14 ] = {
		0xB8, 0x44, 0x33, 0x22, 0x11, 
		0x6A, 0xFE, 0xFF, 0xD0,
		0xE9, 0x36, 0x33, 0x22, 0x11
	};

	*( uintptr_t * )&suspend_shellcode[ 1 ] = ( uintptr_t )GetProcAddress( GetModuleHandleA( "KERNEL32" ), "SuspendThread" );


	auto alloc = VirtualAllocEx( hProc, 0, sizeof( suspend_shellcode ), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE );
	if ( !alloc )
	{
		puts( "Failed to allocate mem" );
		return false;
	}

	*( uintptr_t * )&suspend_shellcode[ 0xA ] = ( uintptr_t )( entrypoint - ( ( uintptr_t )alloc + 0x9 ) - 5 );


	SIZE_T writ = 0;
	if ( !WriteProcessMemory( hProc, alloc, suspend_shellcode, sizeof( suspend_shellcode ), &writ ) )
	{
		puts( "Failed to write mem" );
		return false;
	}

	return ( uintptr_t )alloc;
}

unsigned char original_bytes[ 5 ] = { 0 };

bool HookEntrypoint( HANDLE hProc, uintptr_t ep, uintptr_t hook )
{
	SIZE_T shenanigans = 0;

	if ( !ReadProcessMemory( hProc, ( void * )ep, original_bytes, sizeof( original_bytes ), &shenanigans ) )
	{
		puts( "Faield to rpm og bytes" );
		return false;
	}

	char hook_bytes[ ] = { 0xE9, 0, 0, 0, 0 };
	//0 0xE9 0 -> 5
	auto hook_dest = ( hook - ep - 5 );


	*( uintptr_t * )&hook_bytes[ 1 ] = hook_dest;
	if ( !WriteProcessMemory( hProc, ( void * )ep, hook_bytes, sizeof( hook_bytes ), &shenanigans ) )
	{
		puts( "Faield to wpm hk bytes" );
		return false;
	}

	return true;
}

bool RestoreEntrypoint( HANDLE hProc, uintptr_t ep )
{
	SIZE_T shenanigans = 0;
	if ( !WriteProcessMemory( hProc, ( void * )ep, original_bytes, sizeof( original_bytes ), &shenanigans ) )
	{
		puts( "Faield to wpm og bytes" );
		return false;
	}

	return true;
}

int main( int argc, char ** argv )
{
	// argv[1] == blackcipher location i guess idk
	// lol.
	const char * target = nullptr;
	if ( argc != 2 )
	{
		printf_s( "using hardcoded path [%s]", BC_PATH );
		target = BC_PATH;
		//printf_s( "Need the target process dumbo\n" );
		//return getchar( );
	}
	else
	{
		target = argv[ 1 ];
	}

	puts( target );
	STARTUPINFOA si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	if ( CreateProcessA( target, 0, 0, 0, 0, CREATE_SUSPENDED, 0, 0, &si, &pi ) )
	{
		//TODO: Need to resume thread until it loads kernel32, then suspend again and we can inject.
		auto ep = GetEntryPoint( pi.hProcess );
		if ( ep == 0 )
		{
			TerminateProcess( pi.hProcess, 0 );
			puts( "Failed." );
			return getchar( );
		}

		auto shell = WriteShellcode( pi.hProcess, ep );
		if ( !shell )
		{
			TerminateProcess( pi.hProcess, 0 );
			puts( "Failed to write shellcode" );
			return getchar( );
		}

		if ( !HookEntrypoint( pi.hProcess, ep, shell ) )
		{
			TerminateProcess( pi.hProcess, 0 );
			puts( "Failed to write hook entrypoint" );
			return getchar( );
		}
		
		ResumeThread( pi.hThread );

		puts( "Hooked, thread resumed, should load all libraries and you can now inject!" );

		Sleep( 1000 );
		if ( !RestoreEntrypoint( pi.hProcess, ep ) )
		{
			TerminateProcess( pi.hProcess, 0 );
			puts( "Failed to write original entrypoint" );
			return getchar( );
		}

		puts( "Should be able to inject by now." );

		//puts( "Inject now and resume yourself, bitch." );
	}
	else
	{
		printf_s( "Create process failed. %d\n", GetLastError( ) );
	}

	WaitForSingleObject( pi.hProcess, INFINITE );
	DWORD thread_exit = 0, process_exit = 0;
	auto ect = GetExitCodeThread( pi.hThread, &thread_exit );
	auto ecp = GetExitCodeProcess( pi.hProcess, &process_exit );
	printf_s( "THREAD EXIT CODE: %X\nPROCESS EXIT CODE: %X\n", ect, ecp );


	CloseHandle( pi.hThread );
	CloseHandle( pi.hProcess );
	return getchar( );
}